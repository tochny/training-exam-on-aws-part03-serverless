from flask import Flask,render_template,redirect,session,Blueprint, abort
from flask import Response
from flask import request,jsonify
from datetime import datetime
import time
import json
import requests

app = Flask(__name__)
scoreboard_url = "https://mtfzu2q4gg.execute-api.us-east-1.amazonaws.com/default"
tester_url = "https://os99wyk3t1.execute-api.us-east-1.amazonaws.com/default"
testing_pair = {
    'en' : {
        'origin' : 'Now you can adjust the shade of the screen from white light to a warm amber with the ability to schedule when the light changes for a personalized reading experience. Kindle Oasis also has an adaptive front light that automatically adjusts the brightness of your screen based on lighting conditions.',
        'zh-TW'  : '現在，您可以將螢幕的陰影從白光調整為溫暖的琥珀色，並能在燈光變化時進行排程，以獲得個人化的閱讀體驗。 Kindle Oasis 也有一個自適應的前燈，可根據照明條件自動調整螢幕亮度。',
        'ja'     : '今度は、白色光から暖かい琥珀色に画面の色合いを調整し、光が変化したときにパーソナライズされた読書体験をスケジュールすることができます。 Kindle Oasis には、照明条件に基づいて画面の明るさを自動的に調整するアダプティブなフロントライトも搭載されています。',
        'es'     : 'Ahora puede ajustar el tono de la pantalla de luz blanca a un ámbar cálido con la capacidad de programar cuándo cambia la luz para una experiencia de lectura personalizada. Kindle Oasis también tiene una luz frontal adaptable que ajusta automáticamente el brillo de la pantalla según las condiciones de iluminación.'
    },
    'zh-TW' : {
        'origin' : '今天天氣真好',
        'en'     : 'Good weather today.',
        'de'     : 'Gutes Wetter heute.',
        'es'     : 'Buen tiempo hoy.'
    }
}
default_Item = {
        "userName": "Foo",
        "hintCount": "None",
        "userScore": "None"
    }
data = default_Item

iTester = {
    'message'    : 'Ready',
    'autoCheck'  : 0,
    'checkCount' : 1,
    'loopCount'  : 0,
    'isOkay'     : 0
    }

@app.route('/_stuff')
def add_numbers():
    username = session.get('username','username')
    data['userName'] = username
    r = json.loads(requests.request(method='get', url=scoreboard_url, data=json.dumps(data)).text)
    quizType = r['item']['quizType']
    score = str(r['item']['userScore']).encode('utf-8').decode('utf-8')#str(r1.get(username+"_score"),encoding = "utf-8")
    startTime = time.strftime('%Y-%m-%d %H:%M:%S (UTC + 8)',time.gmtime(float(r['item']['startTime']) + 8 * 60 * 60))
    Hint = str(r['item']['Hint'])
    if(r['item']['Data'] == "Null"):
        endpoint = ""
    else:
        endpoint = str(json.loads(r['item']['Data'])['address'])
    if r['item']['endTime'] == "None":
        endTime = "None"
    else:
        endTime = time.strftime('%Y-%m-%d %H:%M:%S (UTC + 8)',time.gmtime(float(r['item']['endTime']) + 8 * 60 * 60))

    if iTester['autoCheck'] == 1: 
        iTester['message'] = 'Checking your answer(' + str(iTester['checkCount']) + '/6).'
        if iTester['checkCount'] == 1:
            check_speech = json.loads(requests.request(method = 'get', url = tester_url + '/transcribe?url=' + endpoint).text)
            if check_speech == 'PROCESSING':
                iTester['loopCount'] += 1
                if iTester['loopCount'] > 3:
                    iTester['loopCount'] = 0
                for i in range(iTester['loopCount']):
                    iTester['message'] = iTester['message'] + '.'
            elif check_speech == 'True':
                iTester['checkCount'] += 1
            else:
                iTester['message'] = 'Text to speech function incorrect.'
                iTester['autoCheck'] = 0
                iTester['checkCount'] += 1
        elif iTester['checkCount'] == 2:
            if json.loads(requests.request(method = 'get', url = endpoint + '/translate?sl=en&tl=zh-TW&text=' + testing_pair['en']['origin']).text)['Text'] == testing_pair['en']['zh-TW']:
                iTester['checkCount'] += 1
            else :
                iTester['message'] = 'Transcribe function incorrect.'
        elif iTester['checkCount'] == 3:
            if json.loads(requests.request(method = 'get', url = endpoint + '/translate?sl=en&tl=ja&text=' + testing_pair['en']['origin']).text)['Text'] == testing_pair['en']['ja']:
                iTester['checkCount'] += 1
            else :
                iTester['message'] = 'Transcribe function incorrect.'
        elif iTester['checkCount'] == 4:
            if json.loads(requests.request(method = 'get', url = endpoint + '/translate?sl=en&tl=es&text=' + testing_pair['en']['origin']).text)['Text'] == testing_pair['en']['es']:
                iTester['checkCount'] += 1
            else :
                iTester['message'] = 'Transcribe function incorrect.'
        elif iTester['checkCount'] == 5:
            if json.loads(requests.request(method = 'get', url = endpoint + '/translate?sl=zh-TW&tl=en&text=' + testing_pair['zh-TW']['origin']).text)['Text'] == testing_pair['zh-TW']['en']:
                iTester['checkCount'] += 1
            else :
                iTester['message'] = 'Transcribe function incorrect.'
        elif iTester['checkCount'] == 6:
            if json.loads(requests.request(method = 'get', url = endpoint + '/translate?sl=zh-TW&tl=de&text=' + testing_pair['zh-TW']['origin']).text)['Text'] == testing_pair['zh-TW']['de']:
                iTester['checkCount'] += 1
                iTester['isOkay'] = 1
            else :
                iTester['message'] = 'Transcribe function incorrect.'
        else:
            iTester['message'] = 'Ready'



    return jsonify(quizType=quizType, score=score ,startTime=startTime, endTime=endTime, 
                   hintCount = str(int(r['item']['hintCount'])), Hint = Hint, 
                   endpoint = endpoint, message = iTester['message'])

@app.route('/_admin')
def all_score_admin():
    username_all_score={'None', 'None'}
    return jsonify(result=username_all_score)


@app.route('/')
def login():
    return render_template("login.html")


@app.route('/login', methods=['POST'])
def login_1():
    session['username'] = request.form['login']
    session.permanent = True
    username=session.get('username','username')
    data['userName'] = username
    r = json.loads(requests.request(method='get', url=scoreboard_url, data=json.dumps(data)).text)
    if username not in r['item']['userName']:
        data['userScore'] = 0
        data['hintCount'] = 1
        with open("token.log", "r") as data_file:
            data['userToken'] = data_file.read()
        data['quizType'] = 'Serverless'
        data['startTime'] = str(time.time())
        data['endTime'] = "None"
        data['Hint'] = "None"
        data['Tag'] = "Working"
        data['Data'] = "Null"
        r = json.loads(requests.request(method='post', url=scoreboard_url, data=json.dumps(data)).text)
        print(r)
    else:
        print(r)
    if(username=='ADMIN'):
        return redirect('/admin')
    return redirect('/index')

@app.route('/admin')
def admin():
    return render_template("admin.html")

@app.route('/index')
def index():
    username = session.get('username','username')
    getResult=""
    getHintCount=data['hintCount']
    username_all=[]
    return render_template("index_multi.html",username=username,getHintCount=getHintCount, len = len(username_all), username_all = username_all)

@app.route('/token', methods=['POST'])
def token():
    username = session.get('username','username')
    data['userName'] = username
    r = json.loads(requests.request(method='get', url=scoreboard_url, data=json.dumps(data)).text)
    text = request.form['inputToken']
    if r['item']['userToken'] == text and r['item']['endTime'] == 'None':
        data['userScore'] = int(r['item']['userScore']) + int(100)
        data['hintCount'] = int(r['item']['hintCount'])
        data['endTime'] = str(time.time())
        data['Hint'] = r['item']['Hint']
        data['Tag']  = "Complete"
        data['Data'] = r['item']['Data']
        print(data)
        r = json.loads(requests.request(method='post', url=scoreboard_url, data=json.dumps(data)).text)
        print(r)
    return redirect('/index')

@app.route('/getHint', methods=['POST'])
def getHint():
    username = session.get('username','username')
    data['userName'] = username
    r = json.loads(requests.request(method='get', url=scoreboard_url, data=json.dumps(data)).text)
    if int(r['item']['hintCount']) > 0 and r['item']['endTime'] == 'None':
        data['userScore'] = int(r['item']['userScore']) - int(10)
        data['hintCount'] = int(r['item']['hintCount']) - 1
        data['endTime'] = r['item']['endTime']
        data['Hint'] = "Try using docker."
        data['Tag']  = r['item']['Tag']
        data['Data'] = r['item']['Data']
        r = json.loads(requests.request(method='post', url=scoreboard_url, data=json.dumps(data)).text)
    return redirect('/index')

@app.route('/endpoint', methods=['POST'])
def endpoint():
    username = session.get('username','username')
    data['userName'] = username
    r = json.loads(requests.request(method='get', url=scoreboard_url, data=json.dumps(data)).text)
    text = request.form['inputEndpoint']
    data['userScore'] = int(r['item']['userScore'])
    data['hintCount'] = int(r['item']['hintCount'])
    data['endTime'] = r['item']['endTime']
    data['Hint'] = r['item']['Hint']
    data['Tag']  = "Working"
    data['Data'] = json.dumps({'address' : text})
    print(data)
    r = json.loads(requests.request(method='post', url=scoreboard_url, data=json.dumps(data)).text)
    print(r)
    global iTester
    iTester = {
        'autoCheck'  : 1,
        'checkCount' : 1,
        'loopCount'  : 0,
        'isOkay'     : 0
    }
    return redirect('/index')

@app.route('/check', methods=['GET'])
def check():
    username = session.get('username','username')
    data['userName'] = username
    if(iTester['isOkay']):
        return render_template('index.html')
    else:
        return render_template('404.html')

#def parse_data()

if __name__ == "__main__":
    app.secret_key = "super secret key"
    app.run(host='0.0.0.0', port=80)
